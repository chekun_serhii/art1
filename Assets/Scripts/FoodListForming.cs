using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodListForming : MonoBehaviour
{
    public int[] currentList;
    public List<int[]> ListOfLists;
    private LogForming _logForming;
    
    private const int FoodCount = 28;

    private void Awake()
    {
        ListOfLists = new List<int[]>();
        currentList = new int[FoodCount];
        _logForming = FindObjectOfType<LogForming>();
        
        ClearList();
    }

    public void FormList()
    {
        ListOfLists.Add(currentList);
        _logForming.Recommendations = FormRecommendations();
        _logForming.FormLog();

        ClearList();
    }

    private void ClearList()
    {
        for (var i = 0; i < currentList.Length; i++)
            currentList[i] = 0;
    }

    private List<string> FormRecommendations()
    {
        List<string> recommendations;
        var mostSimilarList = ListOfLists[FindMostSimilarListIndex()];

        if (currentList.SequenceEqual(mostSimilarList))
        {
            var tempListOfLists = ListOfLists;
            tempListOfLists.Remove(mostSimilarList);
            mostSimilarList = tempListOfLists[FindMostSimilarListIndex()];
            recommendations = CreateRecommendationStrings(mostSimilarList);
        }
        else
            recommendations = CreateRecommendationStrings(mostSimilarList);

        return recommendations;
    }

    private int FindMostSimilarListIndex()
    {
        var mostSimilarListIndex = 0;
        var maxSimilaritiesCount = 0;

        for (var i = 0; i < ListOfLists.Count; i++)
        {
            var similaritiesCount = FindSimilaritiesCount(currentList, ListOfLists[i]);
            
            if (maxSimilaritiesCount >= similaritiesCount) continue;
            maxSimilaritiesCount = similaritiesCount;
            mostSimilarListIndex = i;
        }

        return mostSimilarListIndex;
    }

    private int FindSimilaritiesCount(int[] firstList, int[] secondList) => 
        firstList.Where((t, i) => t == secondList[i] && t != 0).Count();

    private List<string> CreateRecommendationStrings(int[] similarList)
    {
        var recommendationCount = 0;
        var recommendationString = new List<string>();

        for (var i = 0; i < currentList.Length && recommendationCount < 2; i++)
        {
            if (similarList[i] != currentList[i] && similarList[i] == 1)
            {
                recommendationString.Add(((Foods)i).ToString());
                recommendationCount++;
            }
        }
        
        return recommendationString;
    }

    private void DebugListPrint(int[] list)
    {
        var log = list.Aggregate("", (current, t) => current + t);
        Debug.Log(log);
    }
}
