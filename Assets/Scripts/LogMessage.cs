using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogMessage : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI customerNameText;
    [SerializeField] private TextMeshProUGUI boughtFoodText;
    [SerializeField] private TextMeshProUGUI recommendationsText;
    
    [NonSerialized] public string CustomerName;
    [NonSerialized] public string BoughtFood;
    [NonSerialized] public string Recommendations;
    
    private void Start()
    {
        customerNameText.text = "Name: " + CustomerName;
        boughtFoodText.text = "Bought: " + BoughtFood;
        recommendationsText.text = "Recommendations: " + Recommendations;
    }
}
