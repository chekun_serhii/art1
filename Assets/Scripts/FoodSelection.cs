using EPOOutline;
using TMPro;
using UnityEngine;

public class FoodSelection : MonoBehaviour
{
    private Outlinable _outlinable;
    private TextMeshProUGUI _foodName;
    private void Start()
    {
        _outlinable = GetComponent<Outlinable>();
        _foodName = GameObject.FindWithTag("FoodName").GetComponent<TextMeshProUGUI>();
    }

    private void OnMouseEnter()
    {
        _outlinable.enabled = true;
        _foodName.text = "This is: " + gameObject.name;
    }
    
    private void OnMouseExit() 
    {
        _outlinable.enabled = false;
        _foodName.text = "This is: ";
    }
}
