using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogForming : MonoBehaviour
{
    [SerializeField] private TMP_InputField customerName;
    [SerializeField] private List<string> boughtFoods;
    [SerializeField] private List<string> recommendations;
    [SerializeField] private TextMeshProUGUI boughtFoodUIList;
    [SerializeField] private TextMeshProUGUI recommendationWindowText;
    [SerializeField] private LogMessage logMessagePrefab;
    [SerializeField] private Transform logListContent;

    public List<string> Recommendations
    {
        set => recommendations = value;
    }
    public void FormLog()
    {
        var newLogMessage = Instantiate(logMessagePrefab, logListContent);
        newLogMessage.CustomerName = customerName.text;
        newLogMessage.BoughtFood = FormStringFromList(boughtFoods);
        newLogMessage.Recommendations = FormStringFromList(recommendations);

        recommendationWindowText.text = "We also recommend to buy: " + FormStringFromList(recommendations);

        CleanUIFoodList();
    }

    public void AddFoodToUIList(string foodName)
    {
        if(boughtFoods.Contains(foodName)) return;
        
        boughtFoods.Add(foodName);
        boughtFoodUIList.text = boughtFoodUIList.text + "\n" + foodName;
    }

    public void RemoveFromUIList(string foodName)
    {
        boughtFoods.Remove(foodName);
        boughtFoodUIList.text = "Cart: \n";
        foreach (var food in boughtFoods)
            boughtFoodUIList.text += food + "\n";
    }

    private void CleanUIFoodList()
    {
        boughtFoodUIList.text = "Cart: ";
        boughtFoods.Clear();
        recommendations.Clear();
    }

    private string FormStringFromList(List<string> stringList)
    {
        var finalString = "";
        for (var i = 0; i < stringList.Count; i++)
        {
            var food = stringList[i];
            
            if (i == boughtFoods.Count - 1)
                finalString += food + ".";
            else
                finalString += food + ", ";
        }
        
        return finalString;
    }
}
