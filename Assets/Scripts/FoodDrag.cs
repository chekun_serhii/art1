using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FoodDrag : MonoBehaviour
{
   private Vector3 _offset;
   private float _mouseZCoord;

   private void OnMouseDown()
   {
      var position = gameObject.transform.position;
      _mouseZCoord = Camera.main.WorldToScreenPoint(position).z;
      _offset = position - GetMouseWorldPos();
   }

   private Vector3 GetMouseWorldPos()
   {
      Vector3 mousePoint = Input.mousePosition;
      mousePoint.z = _mouseZCoord;

      return Camera.main.ScreenToWorldPoint(mousePoint);
   }

   private void OnMouseDrag() => transform.position = GetMouseWorldPos() + _offset;
}
