using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Foods
{
    Bellpepper,
    Broccoli,
    Carrot,
    Chili,
    Corn,
    Cucumber,
    Garlic,
    Leek,
    Onion,
    Mushroom,
    Salad,
    Springonion,
    Potato,
    Turnip,
    Tomato,
    Pumpkin,
    Apple,
    Banana,
    Lemon,
    Orange,
    Cheese,
    Egg,
    Fish,
    Meat,
    Bacon,
    Sausage,
    Bread,
    Flour
}

