using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField] private Foods foodType;
    public Foods FoodType => foodType;

    private Vector3 _startPos;
    private Transform _startParent;

    private void Start()
    {
        _startPos = transform.position;
        _startParent = transform.parent;
        CratesCleaning.OnCrateClean += SetFoodStartPos;
    }

    private void SetFoodStartPos()
    {
        transform.position = _startPos;
        transform.parent = _startParent;

    }

    
}
