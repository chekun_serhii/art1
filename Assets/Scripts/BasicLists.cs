using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLists : MonoBehaviour
{
    private int[] _fruitList;
    private int[] _vegetablesList;
    private int[] _meatList;
    private int[] _wheatLeast;

    private FoodListForming _foodListForming;
    private const int FoodCount = 28;

    private void Start()
    {
        _foodListForming = FindObjectOfType<FoodListForming>();
        
        _fruitList = MakeBasicList(0, 15);
        _vegetablesList = MakeBasicList(16, 19);
        _meatList = MakeBasicList(20, 25);
        _wheatLeast = MakeBasicList(26, 27);
        
        
         _foodListForming.ListOfLists.Add(_fruitList);
         _foodListForming.ListOfLists.Add(_vegetablesList);
         _foodListForming.ListOfLists.Add(_meatList);
         _foodListForming.ListOfLists.Add(_wheatLeast);
    }
    
    private int[] MakeBasicList(int fromIndex, int toIndex)
    {
        var basicList = new int[FoodCount];
        for (var i = 0; i < basicList.Length; i++)
            basicList[i] = 0;

        for (var i = fromIndex; i <= toIndex; i++)
            basicList[i] = 1;
        
        return basicList;
    }
}
