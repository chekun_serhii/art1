using UnityEngine;

public class CratesCleaning : MonoBehaviour
{
    public delegate void CrateClean();
    public static event CrateClean OnCrateClean;
    
    public void CleanCrates() => OnCrateClean?.Invoke();
}
