using System;
using UnityEngine;

public class Crate : MonoBehaviour
{
    [SerializeField] private Transform selectedFoodParent;
    [SerializeField] private Transform startFoodParent;
    private FoodListForming _foodListForming;
    private LogForming _logForming;

    private void Start()
    {
        _foodListForming = FindObjectOfType<FoodListForming>();
        _logForming = FindObjectOfType<LogForming>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        var foodType = other.GetComponent<Food>().FoodType;
        
        _foodListForming.currentList[(int) foodType] = 1;
        _logForming.AddFoodToUIList(other.gameObject.name);
        other.transform.parent = selectedFoodParent;
    }

    private void OnTriggerExit(Collider other)
    {
        var foodType = other.GetComponent<Food>().FoodType;
        
        _foodListForming.currentList[(int) foodType] = 0;
        _logForming.RemoveFromUIList(other.gameObject.name);
        other.transform.parent = startFoodParent;
    }
}
